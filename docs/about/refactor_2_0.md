### 为什么要重构?

这个项目的最初目的是为了个人的练手, 当时对 `web ssh` 比较痴迷, 后来想着自己时间出来, 从那之后就开始往里面添加新功能, 可以从最初版本 `1.0.0` 看出来刚上线的时候就已经有很多的功能了,
越写越大 越写越杂 以致于现在已经有点乱套了.

后面没想会被 `gitee` 推荐 也有了越来越多用户 发现了很多业务设计/技术选型的有着很多的问题 毕竟现在是个云原生的时代 技术迭代非常的迅速 我需要对 `orion-ops`
这个产品的未来规划考虑一下. 做到小而细杜绝大而杂.

### 项目地址

**orion-ops-pro** [gitee](https://gitee.com/lijiahangmax/orion-ops-pro) [github](https://github.com/lijiahangmax/orion-ops-pro)

